import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import StarRating from './components/StarRating.vue'

const app = createApp(App);
const mountedApp = app.mount('#app');